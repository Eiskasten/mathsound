#!/bin/sh
ARCHIVE=WavFile.tar.gz
FOLDER=src/main/java/ch/cheapbit/mathsound
DEPCACHE=./.depcache
CACHE_ARCHIVE="$DEPCACHE/$ARCHIVE"

mkdir -p $DEPCACHE
rm -rf $FOLDER/WavFile
curl http://www.labbookpages.co.uk/audio/files/javaWavFiles/WavFile.tar.gz -o $CACHE_ARCHIVE
tar -xzf $CACHE_ARCHIVE -C $FOLDER
#rm -rf $ARCHIVE
for FILE in $FOLDER/WavFile/*; do
	patch $FILE patches/wav_package.diff
done
