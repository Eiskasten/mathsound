package ch.cheapbit.mathsound.main;

import ch.cheapbit.mathsound.processing.Processor;

/**
 * Created by richi on 17.10.16.
 */
public class Main {

    public static void main(String[] args) {
        if (args.length < 1) {
            System.out.println("You have to specify a Lua script!");
            return;
        }
        Processor.writeWave(args[0]);
    }

}

