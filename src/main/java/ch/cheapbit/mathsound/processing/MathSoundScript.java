package ch.cheapbit.mathsound.processing;

import org.luaj.vm2.LuaFunction;
import org.luaj.vm2.LuaTable;
import org.luaj.vm2.LuaValue;
import org.luaj.vm2.lib.jse.JsePlatform;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by richi on 24.10.16.
 */
public class MathSoundScript {

    private LuaTable script;
    private ArrayList<LuaFunction> functions;
    private int bit;
    private int sampleRate;
    private double duration;

    public MathSoundScript(String file) {
        script = JsePlatform.standardGlobals().loadfile(file).call().checktable();
        functions = new ArrayList<>();
        loadValues();
    }

    private void loadValues() {
        bit = script.get("BIT").optint(Defaults.BIT);
        sampleRate = script.get("SAMPLE_RATE").optint(Defaults.SAMPLE_RATE);
        duration = script.get("DURATION").optdouble(Defaults.DURATION);
        for (LuaValue lv : script.keys()) {
            LuaValue val = script.get(lv);
            if (val.isfunction() && lv.toString().toLowerCase().startsWith("channel"))
                functions.add((LuaFunction) val);
        }
        Collections.sort(functions, (luaFunction, t1) -> luaFunction.toString().toLowerCase().compareTo(t1.toString().toLowerCase()));
    }

    public ArrayList<LuaFunction> getFunctions() {
        return new ArrayList<>(functions);
    }

    public int getBit() {
        return bit;
    }

    public int getSampleRate() {
        return sampleRate;
    }

    public double getDuration() {
        return duration;
    }

    public class Defaults {
        public static final int BIT = 16;
        public static final int SAMPLE_RATE = 44100;
        public static final double DURATION = 10.0;
    }
}
