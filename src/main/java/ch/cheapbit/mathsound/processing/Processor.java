package ch.cheapbit.mathsound.processing;

import ch.cheapbit.mathsound.WavFile.WavFile;
import ch.cheapbit.mathsound.WavFile.WavFileException;
import org.luaj.vm2.LuaDouble;
import org.luaj.vm2.LuaFunction;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by richi on 24.10.16.
 */
public class Processor {

    public static void writeWave(String file) {
        MathSoundScript mss = new MathSoundScript(file);

        if (file.endsWith(".lua"))
            file = file.substring(0, file.length() - 4);

        int sampleRate = mss.getSampleRate();
        double duration = mss.getDuration();
        int bit = mss.getBit();
        int channels = mss.getFunctions().size();
        long numFrames = (long) duration * sampleRate;
        ArrayList<LuaFunction> functions = mss.getFunctions();
        try {
            WavFile wavFile = WavFile.newWavFile(new File(file + ".wav"), channels, numFrames, bit, sampleRate);
            double[][] buffer = new double[channels][100];
            long frameCounter = 0;
            while (frameCounter < numFrames) {
                long remaining = wavFile.getFramesRemaining();
                int toWrite = (remaining > 100) ? 100 : (int) remaining;
                for (int s = 0; s < toWrite; s++, frameCounter++) {
                    for (int fkt = 0; fkt < channels; fkt++) {
                        LuaFunction lf = functions.get(fkt);
                        double xValue = (1.0 * frameCounter) / sampleRate;
                        double yValue = lf.invoke(LuaDouble.valueOf(xValue)).optdouble(1, 0);
                        buffer[fkt][s] = yValue;
                    }
                }
                wavFile.writeFrames(buffer, toWrite);
            }
            wavFile.close();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (WavFileException e) {
            e.printStackTrace();
        }
    }
}
